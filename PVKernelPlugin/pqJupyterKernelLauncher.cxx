/*=========================================================================

   Program: ParaView
   Module:    pqJupyterKernelLauncher.cxx

   Copyright (c) 2005,2006 Sandia Corporation, Kitware Inc.
   All rights reserved.

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/
#include "pqJupyterKernelLauncher.h"

// Qt Includes.
#include <QFileInfo>
#include <QProcessEnvironment>
#include <QtDebug>

// ParaView Includes.
#include <vtkPVOptions.h>
#include <vtkProcessModule.h>

#include <iostream>

// xeus include
#include "PVInterpreter.h"
#include "PVKernelServer.h"
#include "xeus/xkernel.hpp"
#include "xeus/xkernel_configuration.hpp"

//-----------------------------------------------------------------------------
class pqJupyterKernelLauncherInternal
{
public:
  pqJupyterKernelLauncherInternal(const std::string& file_name);
  ~pqJupyterKernelLauncherInternal();
  xeus::xkernel* Kernel;
};

//-----------------------------------------------------------------------------
pqJupyterKernelLauncherInternal::pqJupyterKernelLauncherInternal(const std::string& file_name)
{
  xeus::xconfiguration config = xeus::load_configuration(file_name);
  auto interpreter = std::unique_ptr<PVInterpreter>(new PVInterpreter());
  this->Kernel = new xeus::xkernel(config, "paraview", std::move(interpreter),
    xeus::make_in_memory_history_manager(), nullptr, make_PVKernelServer);
}

//-----------------------------------------------------------------------------
pqJupyterKernelLauncherInternal::~pqJupyterKernelLauncherInternal()
{
  delete this->Kernel;
}

//-----------------------------------------------------------------------------
pqJupyterKernelLauncher::pqJupyterKernelLauncher(QObject* p)
  : QObject(p)
  , Internal(nullptr)
{
}

//-----------------------------------------------------------------------------
void pqJupyterKernelLauncher::onStartup()
{
  QString configFile("");
  auto env = QProcessEnvironment::systemEnvironment();
  if (env.contains("JUPYTER_CONFIG_FILE"))
  {
    configFile = env.value("JUPYTER_CONFIG_FILE", "");
  }

  bool fileExists =
    !configFile.isEmpty() && QFileInfo::exists(configFile) && QFileInfo(configFile).isFile();
  if (!fileExists)
  {
    vtkErrorWithObjectMacro(nullptr,
      "no valid configuration file found ('" << configFile.toStdString().c_str()
                                             << "'). Kernel cannot be launched");
    return;
  }

  this->Internal = new pqJupyterKernelLauncherInternal(configFile.toStdString());
  this->Internal->Kernel->start();
}

//-----------------------------------------------------------------------------
void pqJupyterKernelLauncher::onShutdown()
{
  if (this->Internal != nullptr)
  {
    delete this->Internal;
  }
}
